﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MARCToolApp.Views {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Resource {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resource() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("MARCToolApp.Views.Resource", typeof(Resource).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add.
        /// </summary>
        public static string ButtonAdd {
            get {
                return ResourceManager.GetString("ButtonAdd", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Delete.
        /// </summary>
        public static string ButtonDelete {
            get {
                return ResourceManager.GetString("ButtonDelete", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Update.
        /// </summary>
        public static string ButtonEdit {
            get {
                return ResourceManager.GetString("ButtonEdit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Generate Script.
        /// </summary>
        public static string ButtonGenerateScript {
            get {
                return ResourceManager.GetString("ButtonGenerateScript", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to select a code to delete.
        /// </summary>
        public static string DropdownSelectCodeDelete {
            get {
                return ResourceManager.GetString("DropdownSelectCodeDelete", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to select a code to update.
        /// </summary>
        public static string DropdownSelectCodeUpdate {
            get {
                return ResourceManager.GetString("DropdownSelectCodeUpdate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to FAILED TO GENERATE A SQL ALTER SCRIPT!!!.
        /// </summary>
        public static string ErrorScriptFail {
            get {
                return ResourceManager.GetString("ErrorScriptFail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Back to List.
        /// </summary>
        public static string FooterBackToList {
            get {
                return ResourceManager.GetString("FooterBackToList", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 2018 - Airbenders.
        /// </summary>
        public static string FooterCopyright {
            get {
                return ResourceManager.GetString("FooterCopyright", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Home.
        /// </summary>
        public static string HeaderHome {
            get {
                return ResourceManager.GetString("HeaderHome", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tags.
        /// </summary>
        public static string HeaderMarcTag {
            get {
                return ResourceManager.GetString("HeaderMarcTag", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Source Code.
        /// </summary>
        public static string HeaderSourceCode {
            get {
                return ResourceManager.GetString("HeaderSourceCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to MARC Tool.
        /// </summary>
        public static string HeaderTitle {
            get {
                return ResourceManager.GetString("HeaderTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Customer Support.
        /// </summary>
        public static string IndexCustomerSupport {
            get {
                return ResourceManager.GetString("IndexCustomerSupport", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Changes must be added to the customer support ftp folder (after testing is complete): ftp://ftp.polarislibrary.com/MARCUpdates/..
        /// </summary>
        public static string IndexCustomerSupportSec1 {
            get {
                return ResourceManager.GetString("IndexCustomerSupportSec1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to MARC Standards.
        /// </summary>
        public static string IndexMarc {
            get {
                return ResourceManager.GetString("IndexMarc", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to General Information and Change Announcements.
        /// </summary>
        public static string IndexMarcSec1 {
            get {
                return ResourceManager.GetString("IndexMarcSec1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Source Code Lists page.
        /// </summary>
        public static string IndexMarcSec2 {
            get {
                return ResourceManager.GetString("IndexMarcSec2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Home Page.
        /// </summary>
        public static string IndexTitle {
            get {
                return ResourceManager.GetString("IndexTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to New Code.
        /// </summary>
        public static string LabelNewCode {
            get {
                return ResourceManager.GetString("LabelNewCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Source Code.
        /// </summary>
        public static string SourceCodeTitle {
            get {
                return ResourceManager.GetString("SourceCodeTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Code.
        /// </summary>
        public static string TableHeaderCode {
            get {
                return ResourceManager.GetString("TableHeaderCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Column.
        /// </summary>
        public static string TableHeaderColumn {
            get {
                return ResourceManager.GetString("TableHeaderColumn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to MARC Code Table.
        /// </summary>
        public static string TableHeaderTable {
            get {
                return ResourceManager.GetString("TableHeaderTable", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Value.
        /// </summary>
        public static string TableHeaderValue {
            get {
                return ResourceManager.GetString("TableHeaderValue", resourceCulture);
            }
        }
    }
}
