﻿using MARCToolApp.Models;
using MARCToolApp.Models.Enums;
using MARCToolApp.Models.ViewModels;
using MARCToolApp.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MARCToolApp.Repositories
{
    public class RepositorySourceCode : IRepositorySourceCode
    {
        #region Private Members
        private readonly PolarisContext _dbContext;

        private IEnumerable<MARCCodeTableMap> _MarcTables;
        #endregion

        public RepositorySourceCode(PolarisContext dbContext) 
        {
            _dbContext = dbContext;
        }

        public async Task<IEnumerable<SourceCodeModel>> Get()
        {
            return await ReadJson();
        }

        public async Task<IEnumerable<TableModel>> Get(SourceCodeModel sourceCode, bool loadColumns, bool loadCodes)
        {
            List<TableModel> tables = new List<TableModel>();

            if (sourceCode.Tags == null || sourceCode.Tags.Count == 0)
            {
                return tables;
            }

            if (_MarcTables == null || _MarcTables.Count() == 0)
            {
                _MarcTables = await LoadTablesBySourceCode(sourceCode);
            }

            foreach (var table in _MarcTables)
            {
                if (string.IsNullOrEmpty(table.MARCCodeTable))
                {
                    continue;
                }

                var columns = (loadColumns ? await LoadColumns(table.MARCCodeTable) : null);

                var codes = (loadCodes ? (new List<string>( await LoadCodesByTableId(table.MARCCodeTableId))) : null);

                tables.Add(new TableModel
                {
                    MARCCodeTable = table.MARCCodeTable,
                    MARCCodeTableId = table.MARCCodeTableId,
                    MARCCodeTableDisplay = table.MARCCodeTableDisplay,
                    Columns = columns,
                    Codes = codes
                });
            }

            return tables;
        }

        public async Task<TableModel> GetRowValueByCode(string tableName, int tableId, string code)
        {
            var table = new TableModel
            {
                MARCCodeTable = tableName,
                MARCCodeTableId = tableId
            };

            table.Codes = await LoadCodesByTableName(tableName);

            table.Columns = await LoadColumns(tableName);

            var conn = _dbContext.Database.GetDbConnection();
            try
            {
                await conn.OpenAsync();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = $"select * from {tableName} with (nolock) where Code = N'{code}'";
                        cmd.CommandType = CommandType.Text;

                        using (var reader = cmd.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                foreach (var col in table.Columns)
                                {
                                    var i = reader.GetOrdinal($"{col.Name}");

                                    if (reader.GetFieldType(i) == typeof(Boolean))
                                    {
                                        col.Value = reader.GetBoolean(i) ? "1" : "0";
                                    }
                                    else
                                    {
                                        col.Value = (reader.GetValue(i) ?? "").ToString();
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                }
            finally
            {
                conn.Close();
            }

            return table;
        }

        public void Dispose()
        {
            GC.Collect();
            //GC.SuppressFinalize(this);
        }

        #region Private Methods
        private async Task<IEnumerable<SourceCodeModel>> ReadJson()
        {
            var sourcecodes = new List<SourceCodeModel>();

            using (StreamReader sr = new StreamReader("sourcecode.json"))
            {
                var json = await sr.ReadToEndAsync();
                sourcecodes = JsonConvert.DeserializeObject<List<SourceCodeModel>>(json);
            }

            return sourcecodes;
        }

        private async Task<IEnumerable<MARCCodeTableMap>> LoadTablesBySourceCode(SourceCodeModel sourceCode)
        {
            try
            {
                var queryString = "";
                if (sourceCode.CodeId.Equals((int)SourceCode.TaxonomicClassification))
                {
                    queryString = $"select MARCCodeTableID, MARCCodeTable, MARCCodeTableDisplay from MARCCodeTableMap with (nolock) where MarcCodeTableId = 23";
                }
                else
                {
                    var common_sql = "select distinct mffd.MARCCodeTableID, mctm.MARCCodeTable, mctm.MARCCodeTableDisplay " +
                                " from MARCFixedFieldMap mffm with (nolock) " +
                                " inner join MARCTagDefinitions mtd  with (nolock) on (mffm.MARCTagDefinitionID = mtd.MARCTagDefinitionID) " +
                                " inner join MARCFixedFieldDefinitions mffd with (nolock) on (mffm.FixedFieldDefinitionID = mffd.FixedFieldDefinitionID) " +
                                " inner join MARCCodeTableMap mctm with (nolock) on (mffd.MARCCodeTableID = mctm.MARCCodeTableID) " +
                                " where mffd.MARCCodeTableID IS NOT NULL and mffm.Subfield is not null and mctm.Obsolete = 0 ";

                    var bib_tags = sourceCode.Tags.Where(t => t.Format == MarcRecordFormat.Bibliographic).Select(t => t.Tag);
                    var bib_tagIds = String.Join(",", bib_tags.ToArray());

                    var bib_subfields = sourceCode.Tags.Where(t => t.Format == MarcRecordFormat.Bibliographic).Select(t => $"'{t.Subfield}'").Distinct();
                    var bib_subfieldValues = String.Join(",", bib_subfields.ToArray());

                    var auth_tags = sourceCode.Tags.Where(t => t.Format == MarcRecordFormat.Authority).Select(t => t.Tag);
                    var auth_tagIds = String.Join(",", auth_tags.ToArray());

                    var auth_subfields = sourceCode.Tags.Where(t => t.Format == MarcRecordFormat.Authority).Select(t => $"'{t.Subfield}'").Distinct();
                    var auth_subfieldValues = String.Join(",", auth_subfields.ToArray());

                    if (!string.IsNullOrEmpty(bib_tagIds))
                    {
                        queryString = $"{common_sql} and mtd.MARCRecordFormatID = {(int)MarcRecordFormat.Bibliographic} " +
                                $"and mtd.TagNumber in ({bib_tagIds}) and mffm.Subfield in ({bib_subfieldValues})";
                    }
                    if (!string.IsNullOrEmpty(auth_tagIds))
                    {
                        if (!string.IsNullOrEmpty(bib_tagIds))
                        {
                            queryString += " union ";
                        }
                        queryString += $"{common_sql} and mtd.MARCRecordFormatID = {(int)MarcRecordFormat.Authority} " +
                                $" and mtd.TagNumber in ({auth_tagIds}) and mffm.Subfield in ({auth_subfieldValues})";
                    }
                }

                var tables = await _dbContext.MarcCodeTableMaps
                    .AsNoTracking()
                    .FromSql(queryString)
                    .Distinct()
                    .ToListAsync();

                return tables;
            }
            catch (Exception e)
            {
                return new List<MARCCodeTableMap>();
            }
        }

        private async Task<List<ColumnModel>> LoadColumns(string columnTableName)
        {
            List<ColumnModel> columns = new List<ColumnModel>();
            var conn = _dbContext.Database.GetDbConnection();

            try
            {
                await conn.OpenAsync();

                // Specify the Catalog, Schema, Table Name, Table Type to get 
                // the specified table(s).
                // For the array, 0-member represents Catalog; 1-member represents Schema; 
                // 2-member represents Table Name; 3-member represents Table Type. 
                string[] tableRestrictions = new string[] { null, null, columnTableName, null };

                var pk_column_name = "";
                DataTable table_schema = conn.GetSchema("IndexColumns", tableRestrictions);
                foreach (DataRow pk in table_schema.Rows)
                {
                    var pk_index_name = pk[2];
                    pk_index_name = pk["INDEX_NAME"];
                    if (pk_index_name.ToString().ToUpper().StartsWith("PK_"))
                    {
                        pk_column_name = pk["COLUMN_NAME"].ToString();
                        break;
                    }
                }

                var table_columns = conn.GetSchema("Columns", tableRestrictions);
                //var dataColumns = table_columns.Columns.Cast<DataColumn>();
                //foreach(var col in dataColumns)
                //{
                //    if (pk_column_name.ToUpper() == col.ColumnName.ToUpper())
                //    {
                //        // skip PK_ column
                //        continue;
                //    }

                //    columns.Add(new ColumnModel
                //    {
                //        Name = col.ColumnName,
                //        Type = col.DataType.ToString(),
                //        MaxLength = col.MaxLength.ToString(),
                //        DataType = col.DataType.ToString(),
                //        AllowNull = col.AllowDBNull,
                //        Hide = (col.ColumnName.ToUpper() == IgnoredColumn.LocallyDefinedFlag.ToString().ToUpper() ||
                //                col.ColumnName.ToUpper() == IgnoredColumn.ModifiedFlag.ToString().ToUpper() ||
                //                col.ColumnName.ToUpper() == IgnoredColumn.ObsoleteFlag.ToString().ToUpper() ||
                //                col.ColumnName.ToUpper() == IgnoredColumn.UserNotes.ToString().ToUpper() ||
                //                col.ColumnName.ToUpper() == IgnoredColumn.Code.ToString().ToUpper())
                //    });
                //}
                foreach (DataRow row in table_columns.Rows)
                {
                    var col_name = row["COLUMN_NAME"].ToString();
                    var col_type = row["DATA_TYPE"].ToString();
                    var col_length = (col_type.ToUpper() == "BIT") ? "1" :
                            (col_type.ToUpper() == "INT" ? "10" : row["CHARACTER_MAXIMUM_LENGTH"].ToString());

                    if (pk_column_name.ToUpper() == col_name.ToUpper())
                    {
                        // skip PK_ column
                        continue;
                    }

                    columns.Add(new ColumnModel
                    {
                        Name = row["COLUMN_NAME"].ToString(),
                        Type = row["DATA_TYPE"].ToString(),
                        MaxLength = col_length,
                        DataType = row["DATA_TYPE"],
                        AllowNull = (row["IS_NULLABLE"].ToString().ToUpper() == "YES"),
                        Hide = (row["COLUMN_NAME"].ToString().ToUpper() == IgnoredColumn.LocallyDefinedFlag.ToString().ToUpper() ||
                                row["COLUMN_NAME"].ToString().ToUpper() == IgnoredColumn.ModifiedFlag.ToString().ToUpper() ||
                                row["COLUMN_NAME"].ToString().ToUpper() == IgnoredColumn.ObsoleteFlag.ToString().ToUpper() ||
                                row["COLUMN_NAME"].ToString().ToUpper() == IgnoredColumn.UserNotes.ToString().ToUpper() ||
                                row["COLUMN_NAME"].ToString().ToUpper() == IgnoredColumn.Code.ToString().ToUpper())
                    });
                }
            }
            catch
            { }
            finally
            {
                conn.Close();
            }

            return columns;
        }

        private async Task<List<string>> LoadCodesByTableId(int id)
        {
            var codes = new List<string>();
            var conn = _dbContext.Database.GetDbConnection();
            try
            {
                await conn.OpenAsync();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = $"declare @table nvarchar(255); " +
                            $" select @table = MARCCodeTable from MARCCodeTableMap with (nolock)" +
                            $" where MARCCodeTableID = {id}; " +
                            $" exec(N'select distinct Code from ' + @table + N' with (nolock);')";
                    cmd.CommandType = CommandType.Text;

                    using (var reader = await cmd.ExecuteReaderAsync())
                    {
                        if (reader.HasRows)
                        {
                            while (await reader.ReadAsync())
                            {
                                codes.Add(reader.GetValue(0).ToString());
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
            }
            finally
            {
                conn.Close();
            }
            return codes;
        }

        private async Task<List<string>> LoadCodesByTableName(string tableName)
        {
            var codes = new List<string>();
            var conn = _dbContext.Database.GetDbConnection();

            try
            {
                await conn.OpenAsync();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = $"select distinct Code from {tableName} with (nolock)";
                    cmd.CommandType = CommandType.Text;

                    using (var reader = await cmd.ExecuteReaderAsync())
                    {
                        if (reader.HasRows)
                        {
                            while (await reader.ReadAsync())
                            {
                                codes.Add(reader.GetValue(0).ToString());
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
            }
            finally
            {
                conn.Close();
            }

            return codes;
        }
        #endregion
    }
}
