﻿using MARCToolApp.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MARCToolApp.Repositories.Interfaces
{
    public interface IRepositorySourceCode : IDisposable
    {
        Task<IEnumerable<SourceCodeModel>> Get();
        Task<IEnumerable<TableModel>> Get(SourceCodeModel sourceCode, bool loadColumns, bool loadCodes);
        Task<TableModel> GetRowValueByCode(string tableName, int tableId, string code);
    }
}
