﻿To set up locally:

In IIS add an application pool for MarcTool.
In IIS add a default website for MarcTool. On Security tab, add Everyone and give all permissions.
 
IISreset

In visual studios, select the project. 
Select Properties. Go to Debug. 
For the Web Server Settings: enable windows authentication. Enable anonymous authentication. 
Give the App URL http://localhost/MARCTool. save

To release your changes:

Build and publish MARCToolApp in release, copy files to RD-POLARIS C:\Program Files\Polaris\6.3\polaris-innovative-marctool

If you use a future release, update IIS basic settings for the marc tool path
http://rd-polaris.polarislibrary.com/MarcTool