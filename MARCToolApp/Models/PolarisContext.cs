﻿using Microsoft.EntityFrameworkCore;

namespace MARCToolApp.Models
{
    public class PolarisContext : DbContext
    {
        public PolarisContext(DbContextOptions<PolarisContext> options)
            :base(options)
        {
           
        }

        public virtual DbSet<MARCCodeTableMap> MarcCodeTableMaps { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MARCCodeTableMap>()
                ////.ToTable("MARCCodeTableMap")
                .HasKey(x => new { x.MARCCodeTableId });
        }
    }
}
