﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MARCToolApp.Models.Enums
{
    public enum IgnoredColumn
    {
        ObsoleteFlag,
        LocallyDefinedFlag,
        ModifiedFlag,
        UserNotes,
        Code
    }

    public enum MarcRecordFormat
    {
        Bibliographic = 1,
        Authority = 2,
        Holdings,
        Classification,
        Community
    }

    public enum SourceCode
    {
        AbbreviatedTitle = 1,
        AccessRestrictionTerm = 2,
        AvailabilityStatusCode = 3,
        CaptionAbbreviation = 4,
        CartographicData = 5,
        CitationScheme = 6,
        ClassificationScheme = 7,
        ContentAdviceClassification = 8,
        CopyrightAndLegalDepositNumber = 9,
        CountryCodeAndTerm = 10,
        CurriculumObjectiveCodeAndTerm = 11,
        DateAndTimeScheme = 12,
        DescriptionConvention = 13,
        EntityType = 14,
        FingerprintScheme = 15,
        Format = 16,
        FrequencyOfIssueCodeAndTerm = 17,
        FunctionTerm = 18,
        GenderCodeAndTerm = 19,
        GenreFormCodeAndTerm = 20,
        GeographicAreaCodeAndTerm = 21,
        HoldingsScheme = 22,
        HumanServicesCode = 23,
        LanguageCodeAndTerm = 24,
        MusicalCompositionFormCode = 25,
        MusicalIncipitScheme = 26,
        MusicalInstrumentationAndVoiceCodeAndTerm = 27,
        NameAndTitleAuthority = 28,
        NationalBibliographyNumber = 29,
        OccupationTerm = 30,
        OrganizationCode = 31,
        PriceTypeCode = 32,
        ProgramCode = 33,
        RelatorAndRoleCodeAndTerm = 34,
        ResourceActionTerm = 35,
        ScriptCodeAndTerm = 36,
        StandardIdentifier = 37,
        SubjectCategoryCode = 38,
        SubjectHeadingAndTerm = 39,
        TargetAudienceCodeAndTerm = 40,
        TaxonomicClassification = 41,
        ThematicIndexCode = 42,
        TranscriptionSchemeCode = 43
    }
}
