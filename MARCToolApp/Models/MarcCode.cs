﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MARCToolApp.Models
{
    public class MarcCode
    {
        public string Code { get; set; }
        public MARCCodeTableMap MARCCodeTableMap { get; set; }
    }
}
