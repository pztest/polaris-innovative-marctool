﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MARCToolApp.Models
{
    public class MARCCodeTableMap
    {
        public int MARCCodeTableId { get; set; }
        public string MARCCodeTable { get; set; }
        public string MARCCodeTableDisplay { get; set; }
    }
}
