﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MARCToolApp.Models.ViewModels
{
    [Serializable]
    public class SourceCodeModel
    {
        public int CodeId { get; set; }
        public string CodeName { get; set; }
        public string Url { get; set; }
        public string SelectedCode { get; set; }
        public int SelectedCodeMaxLength { get; set; }
        public List<TagModel> Tags { get; set; }
        //public List<MARCCodeTable> MarcCodeTables { get; set; }
        public List<TableModel> CodeTables { get; set; }
    }
}
