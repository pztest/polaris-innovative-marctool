﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MARCToolApp.Models.ViewModels
{
    [Serializable]
    public class MarcCodeEditViewTabelModel
    {
        public TableModel TableModel { get; set; }
        public int TableIndex { get; set; }
    }
}
