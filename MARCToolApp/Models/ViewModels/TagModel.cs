﻿using MARCToolApp.Models.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MARCToolApp.Models.ViewModels
{
    [Serializable]
    public class TagModel
    {
        [Required]
        public MarcRecordFormat Format { get; set; }
        [Required]
        public int Tag { get; set; }
        [Required]
        public string Subfield { get; set; }
    }
}
