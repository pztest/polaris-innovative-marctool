﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace MARCToolApp.Models.ViewModels
{
    [Serializable]
    public class TableModel
    {
        [Required]
        public int MARCCodeTableId { get; set; }
        [Required]
        public string MARCCodeTable { get; set; }
        public string MARCCodeTableDisplay { get; set; }
        //public MARCCodeTableMap MarcTable { get; set; }
        public List<ColumnModel> Columns { get; set; }
        public List<string> Codes { get; set; }
    }
}
