﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MARCToolApp.Models
{
    [Serializable]
    public class ColumnModel
    {
        [Required]
        public string Name { set; get; }
        public string Type { set; get; }
        [Required]
        public string Value { set; get; }
        [Required]
        public bool AllowNull { set; get; }
        public string MaxLength { set; get; }
        public object DataType { set; get; }
        public bool Hide { set; get; }
    }
}
