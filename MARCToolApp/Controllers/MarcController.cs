﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MARCToolApp.Models;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System.Data;
using MARCToolApp.Models.ViewModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MARCToolApp.Controllers
{
    [Authorize]
    public class MarcController : Controller
    {
        private readonly PolarisContext _dbContext;
        //private readonly SqlConnection _conn;
        private readonly IConfiguration _config;
        private string connectionString;

        public MarcController(PolarisContext dbContext, IConfiguration config)
        {
            _dbContext = dbContext;
            _config = config;

            connectionString = _config.GetConnectionString("DefaultConnection");

            //var connString = _config.GetConnectionString("DefaultConnection");
            //_conn = new SqlConnection(connString);
            //_conn = conn;
        }

        // GET: /<controller>/
        public async Task<IActionResult> Index()
        {
            List<TableModel> tables = new List<TableModel>();

            //var marc_tables = _dbContext.MarcCodeTables;
            var marc_tables = await (from e in _dbContext
                                     .Set<MARCCodeTableMap>()
                                     .AsNoTracking() select e)
                                     .ToListAsync();
            using (var conn = new SqlConnection(connectionString))
            {
                conn.Open();

                foreach (var marc_table in marc_tables)
                {
                    var columns = GetColumns(marc_table.MARCCodeTable, conn);

                    tables.Add(new TableModel
                    {
                        MARCCodeTable = marc_table.MARCCodeTable,
                        MARCCodeTableDisplay = marc_table.MARCCodeTableDisplay,
                        MARCCodeTableId = marc_table.MARCCodeTableId,
                        //MarcTable = marc_table,
                        Columns = columns
                    });
                }
            }


            ////var table_names = MarcTables();
            //var table_names = await GetTables();
            //using (var conn = new SqlConnection(connectionString))
            //{
            //    conn.Open();

            //    foreach (var table_name in table_names)
            //    {
            //        var columns = GetColumns(table_name.TableName, conn);

            //        tables.Add(new TableModel
            //        {
            //            Name = table_name.TableName,
            //            Columns = columns
            //        });
            //    }
            //}

            return View(tables);
        }

        #region Private Methods
        //private void GetColumns(DataTable table, SqlConnection conn)
        private List<ColumnModel> GetColumns(string columnTableName, SqlConnection conn)
        {
            List<ColumnModel> columns = new List<ColumnModel>();

            // Specify the Catalog, Schema, Table Name, Table Type to get 
            // the specified table(s).
            // You can use four restrictions for Table, so you should create a 4 members array.
            string[] tableRestrictions = new string[4];
            // For the array, 0-member represents Catalog; 1-member represents Schema; 
            // 2-member represents Table Name; 3-member represents Table Type. 
            // Now we specify the Table Name of the table what we want to get schema information.
            tableRestrictions[2] = columnTableName;

            var table = conn.GetSchema("Columns", tableRestrictions);
            //var schema = conn.GetSchema("Columns");
            foreach (DataRow row in table.Rows)
            {
                //foreach (DataColumn col in table.Columns)
                //{
                //    var length = col.MaxLength;
                //    var datatype = col.DataType;
                //    var name = col.ColumnName;
                //}

                //var datatype = row["DATA_TYPE"].Equals(typeof(string));
                ////var length = row["CHARACTER_MAXIMUM_LENGTH"].ToString();
                //var length = row[8].ToString();

                columns.Add(new ColumnModel
                {
                    Name = row["COLUMN_NAME"].ToString(), //row[3].ToString(),
                    Type = row["DATA_TYPE"].ToString(),// row[7].ToString(),
                    MaxLength = row["CHARACTER_MAXIMUM_LENGTH"].ToString(),
                    DataType = row["DATA_TYPE"],
                });
            }

            return columns;
        }
        private List<DataColumn> ShowDataTable(DataTable table, Int32 length)
        {
            List<DataColumn> columns = new List<DataColumn>();
            //foreach (DataColumn col in table.Columns)
            //{
            //    columns.Add(col);
            //    //Console.Write("{0,-" + length + "}", col.ColumnName);
            //}
            ////Console.WriteLine();

            //foreach (DataRow row in table.Rows)
            //{
            //    foreach (DataColumn col in table.Columns)
            //    {
            //        columns.Add(col);
            //        //if (col.DataType.Equals(typeof(DateTime)))
            //        //    Console.Write("{0,-" + length + ":d}", row[col]);
            //        //else if (col.DataType.Equals(typeof(Decimal)))
            //        //    Console.Write("{0,-" + length + ":C}", row[col]);
            //        //else
            //        //    Console.Write("{0,-" + length + "}", row[col]);
            //    }
            //    //Console.WriteLine();
            //}
            foreach (DataRow row in table.Rows)
            {
                columns.Add(new DataColumn
                {
                    ColumnName = row[3].ToString(),
                    //MaxLength = int.Parse(row[3].ToString()),
                    //DataType = row[2].ToString(),
                });
            }

            return columns;
        }
        //private void ShowDataTable(DataTable table)
        //{
        //    ShowDataTable(table, 14);
        //}
        //private static void ShowColumns(DataTable columnsTable)
        //{
        //    var selectedRows = from info in columnsTable.AsEnumerable()
        //                       select new
        //                       {
        //                           TableCatalog = info["TABLE_CATALOG"],
        //                           TableSchema = info["TABLE_SCHEMA"],
        //                           TableName = info["TABLE_NAME"],
        //                           ColumnName = info["COLUMN_NAME"],
        //                           DataType = info["DATA_TYPE"]
        //                       };

        //    Console.WriteLine("{0,-15}{1,-15}{2,-15}{3,-15}{4,-15}", "TableCatalog", "TABLE_SCHEMA",
        //        "TABLE_NAME", "COLUMN_NAME", "DATA_TYPE");
        //    foreach (var row in selectedRows)
        //    {
        //        Console.WriteLine("{0,-15}{1,-15}{2,-15}{3,-15}{4,-15}", row.TableCatalog,
        //            row.TableSchema, row.TableName, row.ColumnName, row.DataType);
        //    }
        //}
        private List<string> MarcTables()
        {
            var tables = new List<string>();
            using (var conn = new SqlConnection(connectionString))
            {
                conn.Open();

                var cmd = new SqlCommand {
                    //CommandText = "select Name from sys.tables with (nolock) where name like 'marc%codes%'",
                    CommandText = "select Name from sys.tables with (nolock) where name like '%request%'",
                    CommandType = CommandType.Text,
                    Connection = conn
                };

                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var table_name = reader.GetValue(0);
                    tables.Add(table_name.ToString());
                }
            }

            return tables;
        }
        //private async Task<IEnumerable<MARCCodeTableMap>> GetTables()
        //{
        //    return await _dbContext.MarcCodeTables.FromSql("select Name As TableName from sys.tables with (nolock) where name like 'marc%codes%'").ToArrayAsync();
        //}
        #endregion
    }
}
