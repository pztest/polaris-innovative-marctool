﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MARCToolApp.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using MARCToolApp.Models.Enums;
using MARCToolApp.Repositories.Interfaces;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MARCToolApp.Controllers
{
    [Authorize]
    public class MarcCodeController : Controller
    {
        #region Private Members
        private IRepositorySourceCode _repoSourceCode;
        #endregion

        public MarcCodeController(IRepositorySourceCode repoSourceCode)
        {
            _repoSourceCode = repoSourceCode;
        }

#region Actions
        // GET: /<controller>/
        public async Task<IActionResult> Index()
        {
            var list = await Get();
            return View(list);
        }
        // GET: /<controller>/
        public async Task<IActionResult> Add(int id)
        {
            try
            {
                var sourceCode = await GetSourceCode(id);

                var tables = await Get(sourceCode, true, false);

                sourceCode.CodeTables = tables.ToList();

                if (tables.Count() > 0)
                {
                    foreach (var colum in tables.First().Columns)
                    {
                        if (colum.Name.ToUpper() == IgnoredColumn.Code.ToString().ToUpper())
                        {
                            try
                            {
                                sourceCode.SelectedCodeMaxLength = int.Parse(colum.MaxLength);
                            }
                            catch
                            {
                                sourceCode.SelectedCodeMaxLength = 10;
                            }
                            break;
                        }
                    }
                }

                return View(sourceCode);
            }
            catch
            {
                return View();
            }
        }
        [HttpPost]
        public ActionResult AddWithModel(SourceCodeModel sourceCode)
        {
            if (sourceCode.CodeTables == null)
            {
                throw new Exception("Invalid model data. SourceCodeModel.CodeTables is null");
            }

            if (string.IsNullOrWhiteSpace(sourceCode.SelectedCode))
            {
                throw new Exception("Invalid model data. SourceCodeModel.SelectedCode is invalid");
            }

            //if (ModelState.IsValid)
            //{
                var statement = "";
                foreach (var table in sourceCode.CodeTables)
                {
                    var sql = FormatInsertSql(table, sourceCode.SelectedCode);

                    statement += sql;
                }

                //return RedirectToAction("Script",
                //    new
                //    {
                //        sourceCodeName = sourceCode.CodeName,
                //        sqlStatement = statement,
                //        tables = new List<TableModel>(sourceCode.CodeTables)
                //    });

                ViewData["SourceCodeName"] = sourceCode.CodeName;
                ViewData["SqlStatement"] = statement;

                return View("Script");
            //}

            //throw new Exception($"Invalid ModelState. {ModelState.ToString()}");
        }
        // GET: /<controller>/
        public async Task<IActionResult> Edit(int id)
        {
            try
            {
                var sourceCode = await GetSourceCode(id);

                var tables = await Get(sourceCode, true, true);

                var viewTables = new List<MarcCodeEditViewTabelModel>();
                var i = 0;
                foreach (var table in tables)
                {
                    viewTables.Add(new MarcCodeEditViewTabelModel
                    {
                        TableModel = table,
                        TableIndex = i
                    });
                    i++;
                }

                ViewData["SourceCodeName"] = sourceCode.CodeName;
                return View(viewTables);
            }
            catch
            {
                return View();
            }
        }
        public async Task<ActionResult> GetTableValues(string tablename, int tableid, int tableindex, string code)
        {
            var viewTable = new MarcCodeEditViewTabelModel { TableIndex = tableindex };

            viewTable.TableModel = await _repoSourceCode.GetRowValueByCode(tablename, tableid, code);

            return PartialView("_EditTable", viewTable);
        }
        [HttpPost]
        public ActionResult EditWithModel(List<TableModel> codeTables, string sourceCodeName)
        {
            var statement = "";

            foreach(var table in codeTables)
            {
                statement += FormatUpdateSql(table);
            }

            ViewData["SourceCodeName"] = sourceCodeName;
            ViewData["SqlStatement"] = statement;

            return View("Script");
        }
        // GET: /<controller>/
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var sourceCode = await GetSourceCode(id);
                var tables = await Get(sourceCode, false, true);

                ViewData["SourceCodeName"] = sourceCode.CodeName;

                return View(tables);
            }
            catch (Exception e)
            {
                return RedirectToAction("Index");
            }
        }
        [HttpPost]
        public ActionResult DeleteWithModel(List<TableModel> CodeTables, string sourceCodeName)
        {
            if (CodeTables == null)
            {
                throw new Exception("Invalid model data. SourceCodeModel.CodeTables is null");
            }

            var sql_statement = "";
            try
            {
                foreach (var table in CodeTables)
                {
                    sql_statement += FormatDeleteSql(table);
                }
            }
            catch
            {
                sql_statement = Views.Resource.ErrorScriptFail;
            }

            ViewData["SourceCodeName"] = sourceCodeName;
            ViewData["SqlStatement"] = sql_statement;

            return View("Script");
        }
        #endregion
        #region Private Methods
        private async Task<SourceCodeModel> GetSourceCode(int id)
        {
            var list = await Get();
            var sourceCode = list.Single(c => c.CodeId == id);
 
            return sourceCode;
        }
        private async Task<IEnumerable<TableModel>> Get(SourceCodeModel sourceCode, bool loadColumns, bool loadCodes)
        {
            return await _repoSourceCode.Get(sourceCode, loadColumns, loadCodes);

            //List<TableModel> tables = new List<TableModel>();

            //if (sourceCode.Tags == null || sourceCode.Tags.Count == 0)
            //{
            //    return tables;
            //}

            //if (_MarcTables == null || _MarcTables.Count() == 0)
            //{
            //    _MarcTables = await LoadTablesBySourceCode(sourceCode);
            //}

            //using (var conn = new SqlConnection(_connectionString))
            //{
            //    conn.Open();

            //    foreach (var table in _MarcTables)
            //    {
            //        if (string.IsNullOrEmpty(table.MARCCodeTable))
            //        {
            //            continue;
            //        }

            //        var columns = (loadColumns ? LoadColumns(table.MARCCodeTable, conn) : null);
            //        var codes = (loadCodes ? (new List<string>(LoadCodesByTableId(table.MARCCodeTableId, conn))) : null);

            //        tables.Add(new TableModel
            //        {
            //            MARCCodeTable = table.MARCCodeTable,
            //            MARCCodeTableId = table.MARCCodeTableId,
            //            MARCCodeTableDisplay = table.MARCCodeTableDisplay,
            //            Columns = columns,
            //            Codes = codes
            //        });
            //    }
            //}

            //return tables;
        }
        private async Task<IEnumerable<SourceCodeModel>> Get()
        {
            return await _repoSourceCode.Get();
        }
        #endregion  
        #region Helper - Format SQL
        private string FormatInsertSql(TableModel table, string newCode)
        {
            string sql = "";

            sql = $"if exists(select * from Polaris.{table.MARCCodeTable} with (nolock) where Code = N'{newCode}') {Environment.NewLine}" +
                $"\tdelete from {table.MARCCodeTable} where Code = N'{newCode}';{Environment.NewLine}";

            var columns = "";
            var values = "";
            foreach (var column in table.Columns)
            {
                columns += (string.IsNullOrEmpty(columns) ? "" : ", ") + column.Name;

                // apply default values to predefined columns
                var value = (column.Name == IgnoredColumn.LocallyDefinedFlag.ToString() ||
                    column.Name == IgnoredColumn.ObsoleteFlag.ToString() || column.Name == IgnoredColumn.ModifiedFlag.ToString()) ? "0" :
                        (column.Name == IgnoredColumn.Code.ToString() ? newCode : column.Value);

                if (!string.IsNullOrEmpty(value) && (column.Type.ToLower() == "nvarchar" || column.Type.ToLower() == "nchar"))
                {
                    // N'' prefix and quote string/char values 
                    value = NormalizeSqlStringValue(value);
                    value = $"N'{value}'";
                }
                else if (!string.IsNullOrEmpty(value) && (column.Type.ToLower() == "varchar" || column.Type.ToLower() == "char"))
                {
                    // '' quote string/char values
                    value = NormalizeSqlStringValue(value);
                    value = $"'{value}'";
                }

                values += (string.IsNullOrEmpty(values) ? "" : ", ") + (string.IsNullOrEmpty(value) ? "null" : value);
            }

            sql += $"insert into Polaris.{table.MARCCodeTable} {Environment.NewLine} \t({columns}){Environment.NewLine}" +
                $"values {Environment.NewLine}\t({values});{Environment.NewLine}{Environment.NewLine}";

            return sql;
        }
        private string FormatDeleteSql(TableModel table)
        {
            var sql = "";
            if (table.Codes != null 
                && table.Codes.Count() == 1 
                && !string.IsNullOrEmpty(table.Codes[0]))
            {
                var code = table.Codes[0];
                code = NormalizeSqlStringValue(code);
                sql = $"delete from Polaris.{table.MARCCodeTable} {Environment.NewLine}\twhere Code = N'{code}';{Environment.NewLine}{Environment.NewLine}";
            }
            return sql;
        }
        private string FormatUpdateSql(TableModel table)
        {
            string sql = "";

            if (table.Codes == null || table.Codes.Count() == 0 || string.IsNullOrEmpty(table.Codes[0]) ||
                table.Columns == null || table.Columns.Count() == 0)
            {
                return sql;
            }

            //sql = $"if exists(select * from Polaris.{table.MARCCodeTable} with (nolock) where Code = N'{newCode}') {Environment.NewLine}" +
            //    $"\tdelete from {table.MARCCodeTable} where Code = N'{newCode}';{Environment.NewLine}";

            var sets = "";
            //var values = "";
            foreach (var column in table.Columns)
            {
                if (column.Name.ToUpper() == IgnoredColumn.Code.ToString().ToUpper())
                    continue;

                var value = column.Value;

                if (!string.IsNullOrEmpty(value) && (column.Type.ToLower() == "nvarchar" || column.Type.ToLower() == "nchar"))
                {
                    // N'' prefix and quote string/char values 
                    value = NormalizeSqlStringValue(value);
                    value = string.IsNullOrEmpty(value) ? "null" : $"N'{value}'";
                }
                else if (!string.IsNullOrEmpty(value) && (column.Type.ToLower() == "varchar" || column.Type.ToLower() == "char"))
                {
                    // '' quote string/char values
                    value = NormalizeSqlStringValue(value);
                    value = string.IsNullOrEmpty(value) ? "null" : $"'{value}'";
                }
                else
                {
                    if (string.IsNullOrEmpty(value))
                    {
                        value = (column.Type.ToLower() == "bit" ? "0" : "null");
                    }
                }
                
                sets += $"{(string.IsNullOrEmpty(sets) ? "" : ", ")}{Environment.NewLine}\t{column.Name}={value}";
            }

            sql = $"update {Environment.NewLine}\tPolaris.{table.MARCCodeTable} {Environment.NewLine} set {sets}{Environment.NewLine}" +
                $"where {Environment.NewLine}\tCode = N'{table.Codes[0].Trim()}';{Environment.NewLine}{Environment.NewLine}";

            return sql;
        }
        private string NormalizeSqlStringValue(string value)
        {
            var return_value = value.Replace("'", "''", StringComparison.CurrentCulture);

            return return_value;
        }
        #endregion
    }
}
